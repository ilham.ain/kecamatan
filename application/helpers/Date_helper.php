<?php 
	
	function human_date($date) {
		$bulan = [
			'01' => 'Januari',
			'02' => 'Pebruari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'Nopember',
			'12' => 'Desember'
		];

		return date('d', strtotime($date)) . ' ' . $bulan[date('m', strtotime($date))] . ' ' . date('Y', strtotime($date));
	}