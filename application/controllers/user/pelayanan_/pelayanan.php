<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Controller pelayanan umum
*/
class Pelayanan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mypelayanan');

	}

	public function index(){
		$this->load->model('Mypelayanan');
		$data['umum'] = $this->Mypelayanan->get_pelayanan();

		$this->load->view('partial/_header');
		$this->load->view('pelayanan/_umum',$data);
		$this->load->view('partial/_footer');
	}

	public function form(){
		$this->load->model('Mypelayanan');
		$data['find'] = $this->Mypelayanan->find_pelayanan($this->input->get('id_pelayanan'));
		$data['temu'] = $this->Mypelayanan->find_daftar($this->input->get('id'));

		$this->load->view('partial/_header');
		$this->load->view('pelayanan/_form_umum',$data);
		$this->load->view('partial/_footer');
		
	}

	public function buat_boro(){

		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_daftar_boro = $this->input->post('id_daftar_boro');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');
		
		//tabel surat boro
		$id = $this->input->post('id');
		$nik = $this->input->post('username');
		$tujuan = $this->input->post('tujuan');
		$tgl_berangkat = $this->input->post('tgl_berangkat');
		$sampai_tgl = $this->input->post('sampai_tgl');

		//pengikut
		$id_pengikut = $this->input->post('id');
		$tipe_pengikut = $this->input->post('tipe_pengikut');
		$tempat = $this->input->post('tempat_pengikut');
		$nama_pengikut = $this->input->post('nama_pengikut');
		$kelamin_pengikut = $this->input->post('kelamin_pengikut');
		$nik_pengikut = $this->input->post('nik_pengikut');
		$tgl_lahir_pengikut = $this->input->post('tgl_lahir_pengikut');
		

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
			
			);
		$data_boro = array(
			'id' => $id,
			'nik' => $username,
			'tujuan' => $tujuan,
			'keperluan' => $keperluan,
			'tgl_berangkat' => $tgl_berangkat,
			'sampai_tgl' => $sampai_tgl
		);

		$pengikut = array(
			'id' => $id_pengikut,
			'id_surat' => $id,
			'tipe_pengikut' => $tipe_pengikut,
			'nama_pengikut' => $nama_pengikut,
			'tempat_pengikut' => $tempat,
			'tgl_lahir_pengikut' => $tgl_lahir_pengikut,
			'kelamin_pengikut' => $kelamin_pengikut,
			'nik_pengikut' => $nik_pengikut
			
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_boro($data_boro,'surat_boro');
		$this->Mypelayanan->create_boro_pengikut($pengikut,'pengikut');
		redirect('user/konten_/status');
	}

	public function create_pindah(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat pindah
		$id = $this->input->post('id');
		$nik = $this->input->post('username');
		$tujuan = $this->input->post('tujuan');
		$tgl_berangkat = $this->input->post('tgl_berangkat');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
			
			);
		$data_pindah = array(
			'id' => $id,
			'nik' => $nik,
			'tujuan'=> $tujuan,
			'keperluan' => $keperluan,
			'tgl_berangkat' => $tgl_berangkat
		);

		$this->Mypelayanan->create($data, 'daftar');
		$this->Mypelayanan->create_pindah($data_pindah, 'surat_pindah');
		redirect('user/konten_/status');
	}
	

	public function buat_skck(){

		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel skck
		$id_surat =  $this->input->post('id_surat');
		$nik = $this->input->post('username');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
			
			);
		$data_skck = array(
			'id_surat' => $id_surat,
			'nik' => $nik,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_skck($data_skck,'skck');
		redirect('user/konten_/status');

	}

	public function buat_sktm(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('kegunaan');

		//tabel sktm
		$id_surat =  $this->input->post('id_surat');
		$nik = $this->input->post('username');
		$kegunaan = $this->input->post('kegunaan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
			
			);
		$data_sktm = array(
			'id_surat' => $id_surat,
			'nik' => $nik,
			'kegunaan' => $kegunaan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_sktm($data_sktm,'sktm');
		redirect('user/konten_/status');
	}

	public function create_kredit_bank(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel kredit bank
		$id_surat = $this->input->post('id_surat');
		$nik = $this->input->post('username');
		$keterangan = $this->input->post('keterangan');
		$alamat = $this->input->post('alamat');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_kredit_bank = array(
			'id_surat' => $id_surat,
			'nik' => $nik,
			'keterangan' => $keterangan,
			'alamat' => $alamat,
			'keperluan' => $keperluan 
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_kredit_bank($data_kredit_bank,'surat_kredit_bank');
		redirect('user/konten_/status');
	}

	public function create_domisili()
	{
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat domisili
		$id_surat = $this->input->post('id_surat');
		$nik = $this->input->post('username');
		$keperluan = $this->input->post('keperluan');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_domisili = array(
			'id_surat' => $id_surat,
			'nik' => $nik,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan 
		);	

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_domisili($data_domisili,'surat_domisili');
		redirect('user/konten_/status');	
	}

	public function create_imb(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat imb
		$id = $this->input->post('id');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$pekerjaan = $this->input->post('pekerjaan');
		$alamat = $this->input->post('alamat');
		
		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_imb = array(
			'id' => $id,
			'username' => $username,
			'tgl_lahir' => $tgl_lahir,
			'pekerjaan' => $pekerjaan,
			'alamat' => $alamat
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_imb($data_imb, 'surat_imb');
		redirect('user/konten_/status');
	}

	public function create_usaha(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_usaha = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_usaha($data_usaha,'surat_usaha');
		redirect('user/konten_/status');	

	}
	public function create_domisili_penduduk(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_domisili_pendududuk = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_domisili_penduduk($data_domisili_pendududuk,'surat_domisili_penduduk');
		redirect('user/konten_/status');	

	}

	public function create_cari_ktp(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan 
		);

		$data_cari_ktp = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_cari_ktp($data_cari_ktp,'surat_cari_ktp');
		redirect('user/konten_/status');	

	}

	public function create_belum_nikah(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_sebelum_nikah = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_belum_nikah($data_sebelum_nikah,'surat_belum_nikah');
		redirect('user/konten_/status');	

	}

	public function create_dispensasi_nikah(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_dispensasi = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_dispensasi($data_dispensasi,'surat_dispensasi_nikah');
		redirect('user/konten_/status');	

	}

	public function create_ahli_waris(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_ahli_waris = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_ahli_waris($data_ahli_waris,'surat_ahli_waris');
		redirect('user/konten_/status');	

	}

	public function create_domisili_usaha(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_domisili_usaha = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_domisili_usaha($data_domisili_usaha,'surat_domisili_usaha');
		redirect('user/konten_/status');	

	}

	public function create_kehilangan(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_kehilangan = array(
			'id' => $id,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_kehilangan($data_kehilangan,'sk_kehilangan');
		redirect('user/konten_/status');	

	}

	public function create_ijin_keramaian(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id = $this->input->post('id');
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_ijin_keramaian = array(
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_ijin_keramaian($data_ijin_keramaian,'surat_ijin_keramaian');
		redirect('user/konten_/status');	

	}

	public function create_satu_nama(){
		//tabel daftar
		$id_daftar = $this->input->post('id_daftar');
		$id_anggota = $this->input->post('id_anggota');
		$id_pelayanan = $this->input->post('id_pelayanan');
		$username = $this->input->post('username');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$status = $this->input->post('status');
		$posisi = $this->input->post('posisi');
		$desa = $this->input->post('desa');
		$keperluan = $this->input->post('keperluan');

		//tabel surat usaha
		$id_anggota = $this->input->post('id_anggota');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_daftar' => $id_daftar,
			'id_anggota' => $id_anggota,
			'id_pelayanan' => $id_pelayanan,
			'username' => $username,
			'tgl_daftar' => $tgl_daftar,
			'status' => $status,
			'posisi' => $posisi,
			'desa' => $desa,
			'keperluan' => $keperluan
		);

		$data_satu_nama = array(
			'nik' => $username,
			'id_anggota' => $id_anggota,
			'keterangan' => $keterangan,
			'keperluan' => $keperluan
		);

		$this->Mypelayanan->create($data,'daftar');
		$this->Mypelayanan->create_satu_nama($data_satu_nama,'surat_satu_nama');
		redirect('user/konten_/status');	

	}
}

?>