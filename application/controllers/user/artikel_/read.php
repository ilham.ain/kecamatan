<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Artikel Controller
*/
class Read extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
		$this->load->model('Myartikel');
		$data = array('admin' => $this->Myartikel->get_artikel());
	
		$this->load->view('partial/_header');
		$this->load->view('artikel/view', $data);
		$this->load->view('partial/_footer');
	}
}

?>