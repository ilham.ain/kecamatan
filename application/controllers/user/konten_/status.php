<?php 

/**
 * controller status daftar
 */
 class Status extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 	}

 	public function index()
 	{
 		$this->load->model('Mystatus');
		$data['find'] = $this->Mystatus->tampil($this->session->userdata('id_anggota'), $this->input->get('id_pelayanan'));

 		$this->load->view('partial/_header');
 		$this->load->view('konten/status_daftar',$data);
 		$this->load->view('partial/_footer');
 	}
 } ?>