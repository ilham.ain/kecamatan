<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Pengaduan Controller
*/
class Pengaduan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->helper('form','url');
		$this->load->model('Mypengaduan');
	}

	public function index(){
		$this->load->model('Mypengaduan');
		$data['pengaduan'] = $this->Mypengaduan->read();

		$this->load->model('Mypengaduan');
		$data['komentar'] = $this->Mypengaduan->komentar();

		$this->load->view('partial/_header');
		$this->load->view('konten/_pengaduan', $data);
		$this->load->view('partial/_footer');
	}

	  public function tambah(){  
	  	$parent_id = $this->input->post('parent_id');
	  	$tanggal_pengaduan = $this->input->post('tanggal_pengaduan');
		$nama = $this->input->post('nama');
		$kepada = $this->input->post('kepada');
		$komentar = $this->input->post('komentar');
 
		$data = array(
			'nama' => $nama,
			'tanggal_pengaduan' => $tanggal_pengaduan,
			'kepada' => $kepada,
			'komentar' => $komentar,
			'parent_id' => $parent_id
			);
		$this->Mypengaduan->input_data($data,'pengaduan');
		redirect('user/konten_/pengaduan');
	  	
	}
}
?>