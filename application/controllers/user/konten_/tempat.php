<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Tempat dan Waktu Controller
*/
class Tempat extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$this->load->view('partial/_header');
		$this->load->view('konten/tempat');
		$this->load->view('partial/_footer');
	}
}
?>