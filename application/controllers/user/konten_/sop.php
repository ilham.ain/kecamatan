<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* untuk informasi Download SOP
*/
class Sop extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
		$this->load->model('Mysop');
		$data = array('sop' => $this->Mysop->get_sop());
		/*foreach ($data as $a) {
			echo "Nama = ".$a['username']."";
		}*/
	
		$this->load->view('partial/_header');
		$this->load->view('konten/sop', $data);
		$this->load->view('partial/_footer');
	}
}

?>