<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Berita Controller
*/
class Read extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
		$this->load->model('Myberita');
		$data = array('all_berita' => $this->Myberita->get_berita());
	
		$this->load->view('partial/_header');
		$this->load->view('berita/view', $data);
		$this->load->view('partial/_footer');
	}
}

?>