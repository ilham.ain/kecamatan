<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Phpword');
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
    }

	public function Boro($id) {
		$data = $this->db->select('anggota.*')->join('anggota','anggota.id_anggota=daftar.id_anggota')->where('daftar.id_daftar', $id)->get('daftar')->row_array();
		print_r($data);
		$document = $phpWord->loadTemplate('./assets/file_surat/boro.docx');
		$name = 'boro_'.date('Y-m-d').'.docx';
		$document->setValue('nama', $data['nama']);
		$document->saveAs($name);
		rename($name, './assets/file_surat_unduh/' . $name);
		header('Location: ../../assets/file_surat_unduh/'. $name);
	}
}
