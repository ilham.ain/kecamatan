<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Panduan Controller
*/
class Panduan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$this->load->view('basic/menu');
		$this->load->view('konten/panduan');
		$this->load->view('basic/bawah');
	}
}
?>