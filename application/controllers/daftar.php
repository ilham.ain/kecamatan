<?php 

/**
* 
*/
class Daftar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){

		$this->load->view('basic/menu');
		$this->load->view('Daftar');
		$this->load->view('basic/bawah');
	}

	public function tambah(){
		$this->load->model('Mylogin');

		$id_anggota = $this->input->post('id_anggota');
		$nik = $this->input->post('nik');
		$nkk = $this->input->post('nkk');
		$desa = $this->input->post('desa');
		$nama = $this->input->post('nama');
		$kelamin = $this->input->post('kelamin');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$gol_darah = $this->input->post('gol_darah');
		$agama = $this->input->post('agama');
		$status_nikah = $this->input->post('status_nikah');
		$shdrt = $this->input->post('shdrt');
		$pddk = $this->input->post('pddk');
		$pekerjaan = $this->input->post('pekerjaan');
		$ibu = $this->input->post('ibu');
		$ayah = $this->input->post('ayah');
		$kebangsaan = $this->input->post('kebangsaan');
		$username = $this->input->post('nik');
		$password = $this->input->post('nik');

		$data= array(
			'id_anggota' => $id_anggota,
			'nik' => $nik,
			'nkk' => $nkk,
			'desa' => $desa,
			'nama' => $nama,
			'kelamin' => $kelamin,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'gol_darah' => $gol_darah,
			'agama'=> $agama,
			'status_nikah' => $status_nikah,
			'shdrt' => $shdrt,
			'pddk' => $pddk,
			'pekerjaan'=>$pekerjaan,
			'ibu' => $ibu,
			'ayah' => $ayah,
			'kebangsaan' => $kebangsaan,
			'username' =>$username,
			'password' => $password

		);

		$this->Mylogin->daftar($data,'anggota');
		redirect('kecamatan');
	}

}

?>