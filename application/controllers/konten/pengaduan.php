<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Pengaduan Controller
*/
class Pengaduan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){


		$this->load->model('Mypengaduan');
		$data['pengaduan'] = $this->Mypengaduan->read();

		$this->load->view('basic/header');
		$this->load->view('konten/pengaduan', $data);
		$this->load->view('basic/footer');
	}
}
?>