<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Panduan Controller
*/
class Panduan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$this->load->view('basic/header');
		$this->load->view('konten/panduan');
		$this->load->view('basic/footer');
	}
}
?>