<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* untuk informasi Download SOP
*/
class Sop extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('Download');
		$this->load->helper('url');
		$this->load->library('encrypt');
	}

	public function index()
	{
		$this->load->model('Mysop');
		$data = array('sop' => $this->Mysop->get_sop());
	
		$this->load->view('basic/menu');
		$this->load->view('konten/sop', $data);
		$this->load->view('basic/footer');
	}

	public function download()
	{
		$data = 'Download';
		$name = 'SOP.doc';

		force_download($name,$data);
	}
}

?>