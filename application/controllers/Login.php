<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	public function index() {
		if($this->session->userdata('role') == 'user') {
			redirect('home');
		}
		$this->load->view('login');
	}

	public function login() {
		print_r($_SESSION);
		if($this->session->userdata('role') == 'user') {
			redirect('home');
		}
		$result = $this->db->where(['username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))])
		->get('anggota')->row_array();
		if($result) {
			$this->session->set_userdata(['username' => $result['username'], 'nama' => $result['nama'], 'desa' => $result['desa'],'id_anggota' => $result['id_anggota'], 'role' => 'user']);
		} else {
			$this->session->set_flashdata(['login' => 'false']);
		}
		redirect('user');
	}

	public function logout() {
		$this->session->unset_userdata(['username', 'nama','desa','id_anggota', 'role']);
		redirect('kecamatan');
	}
}
