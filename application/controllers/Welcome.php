<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('basic/header');
		$this->load->view('welcome_message');
		$this->load->view('basic/footer');
		$this->load->library('Phpword');
	}
}
