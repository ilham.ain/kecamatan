<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Controller pelayanan umum
*/
class Pelayanan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$this->load->model('Mypelayanan');
		$data['umum'] = $this->Mypelayanan->get_pelayanan();

		$this->load->view('basic/menu');
		$this->load->view('pelayanan/umum',$data);
		$this->load->view('basic/footer');
	}

	public function form(){
		$this->load->model('Mypelayanan');
		$data['find'] = $this->Mypelayanan->find_pelayanan($this->input->get('id_pelayanan'));

		$this->load->view('basic/menu');
		$this->load->view('pelayanan/form_umum',$data);
		$this->load->view('basic/footer');
		
	}
}

?>