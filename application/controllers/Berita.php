<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Berita Controller
*/
class Berita extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Myberita');
	}

	public function index()
	{
		$data = array('all_berita' => $this->Myberita->get_berita());
	
		$this->load->view('basic/menu');
		$this->load->view('berita/view', $data);
		$this->load->view('basic/bawah');
	}

	public function lengkap(){

		$this->load->model('Myberita');
		$data['all_berita'] = $this->Myberita->find_berita($this->input->get('id_berita'));

		$this->load->view('basic/menu');
		$this->load->view('berita/lengkap', $data);
		$this->load->view('basic/bawah');

	}

}

?>