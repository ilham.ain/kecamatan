<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function index()
	{
		$this->load->model('Mysop');
		$data['sop'] = $this->Mysop->get_sop();

		$this->load->model('Myberita');
		$data['all_berita'] = $this->Myberita->get_berita();
		
		$this->load->model('Myartikel');
		$data['admin']= $this->Myartikel->get_artikel();

		$this->load->view('basic/header');
		$this->load->view('index', $data);
		$this->load->view('basic/footer');
	}
}
