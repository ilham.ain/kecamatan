<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('role')) {
			redirect('admin');
		}
	}

	public function index() {
		$result = $this->db->where('nama', $this->session->userdata('kantor'))->get('kelurahan')->row_array();
		$this->load->view('admin/setting', array('result' => $result));
	}

	public function password() {
		$result = $this->db->where('id', $this->session->userdata('id'))->get('admin')->row_array();
		if($result['password'] == md5($this->input->post('password'))){
			if($this->input->post('new_password') == $this->input->post('password_confirm')) {
				$this->db->where('id', $this->session->userdata('id'))->set('password', md5($this->input->post('new_password')))->update('admin');
				$this->session->set_flashdata([
					'icon' => 'check',
					'alert' => 'success',
					'message' => 'Sukses Merubah Password'
				]);
			} else {
				$this->session->set_flashdata([
					'icon' => 'exclamation',
					'alert' => 'danger',
					'message' => 'Konfirmasi Password Tidak Cocok'
				]);
			}
		} else {
			$this->session->set_flashdata([
				'icon' => 'exclamation',
				'alert' => 'danger',
				'message' => 'Password Lama Tidak Cocok'
			]);
		}
		redirect('admin/setting');
	}

	public function info() {
		$this->db->set([
			'lurah' => $this->input->post('lurah'),
			'nip' => $this->input->post('nip'),
			'pangkat' => $this->input->post('pangkat'),
			'alamat' => $this->input->post('alamat'),
			'kode_pos' => $this->input->post('kode_pos'),
			'telepon' => $this->input->post('telepon')
		])->where('nama', $this->session->userdata('kantor'))->update('kelurahan');
		$this->session->set_flashdata([
			'icon' => 'check',
			'alert' => 'success',
			'message' => 'Sukses Merubah Informasi'
		]);
		redirect('admin/setting');
	}
}
