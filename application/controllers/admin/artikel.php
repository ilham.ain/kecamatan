<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$result = $this->db->get('artikel')->result_array();
		$this->load->view('admin/artikel/index', array('result'=> $result));
	}

	public function insert() {
		$config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['encrypt_name']			= true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            print_r($this->upload->display_errors());
        } else  {
        	$this->db->insert('artikel', [
            	'judul' => $this->input->post('judul'),
            	'isi' => $this->input->post('isi'),
            	'gambar' => 'assets/img/'. $this->upload->data('file_name'),
            	'tgl' => date('Y-m-d')
            ]);
            redirect('admin/artikel');
    	}
	}

    public function edit($id) {
        $result = $this->db->where('id_artikel', $id)->get('artikel')->row_array();
        if($result) {
            $this->load->view('admin/artikel/edit', array('result' => $result));
        } else {
            redirect('admin/artikel');
        }
    }

    public function update($id) {
        if($_FILES['gambar']['size'] > 0) {
            $config['upload_path']          = './assets/img/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 2000;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar')) {
                print_r($this->upload->display_errors());
            } else  {
                $this->db->set('gambar', 'assets/img/'. $this->upload->data('file_name'));
            }
        }
        $this->db->set([
            'isi' => $this->input->post('isi'),
            'judul' => $this->input->post('judul')
        ])->where('id_artikel', $id)->update('artikel');
        redirect('admin/artikel');
    }
}
