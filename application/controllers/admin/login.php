<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		if($this->session->userdata('role') == 'admin') {
			redirect('admin/home');
		}
		$this->load->view('admin/login');
	}

	public function login() {
		if($this->session->userdata('role') == 'admin') {
			redirect('admin/home');
		}
		$result = $this->db->where(['username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))])
		->get('admin')->row_array();
		if($result) {
			$this->session->set_userdata(['username' => $result['username'], 'kantor' => $result['kantor'], 'role' => 'admin', 'id' => $result['id']]);
		} else {
			$this->session->set_flashdata(['login' => 'false']);
		}
		redirect('admin');
	}

	public function logout() {
		$this->session->unset_userdata(['username', 'kantor', 'role', 'id']);
		redirect('admin');
	}
}
