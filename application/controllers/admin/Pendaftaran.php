<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('role')) {
			redirect('admin');
		}
	}

	public function index() {
		if($this->session->userdata('kantor') == 'Kecamatan') {
			$this->db->where('daftar.status', 'sudah diverifikasi di Kelurahan');
		} else {
			$this->db->where('daftar.desa', $this->session->userdata('kantor'));
		}

		$result = $this->db->join('pelayanan', 'pelayanan.id_pelayanan=daftar.id_pelayanan')
			->join('anggota', 'anggota.nik=daftar.username')
			->select('id_daftar, pelayanan.nama as pelayanan, nik, anggota.nama as nama, anggota.desa, daftar.tgl_daftar, daftar.status')
		->get('daftar')->result_array();
		$this->load->view('admin/pendaftaran/index', array('result' => $result));
	}

	public function verifikasi($id) {
		if($this->input->post()) {
			if($this->session->userdata('kantor') == 'Kecamatan') {
				$this->db->set([
					'keperluan' => $this->input->post('keperluan'),
					'status' => 'sudah diverifikasi di Kecamatan'
				])->where('id_daftar', $id)->update('daftar');
				redirect('admin/pendaftaran');
			} else {
				$this->db->set([
					'keperluan' => $this->input->post('keperluan'),
					'status' => 'sudah diverifikasi di Kelurahan'
				])->where('id_daftar', $id)->update('daftar');
				redirect('admin/pendaftaran');
			}
		}
		$result = $this->db->where('id_daftar', $id)
			->join('pelayanan', 'pelayanan.id_pelayanan=daftar.id_pelayanan')
			->join('anggota', 'anggota.nik=daftar.username')
			->select('id_daftar, pelayanan.nama as pelayanan, nik, anggota.nama as nama, anggota.desa, daftar.keperluan')
		->get('daftar')->row_array();
		$this->load->view('admin/pendaftaran/verifikasi', array('result' => $result));
	}

	public function cetak($id) {
        $this->load->library('Phpword');
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		$daftar = $this->db->where('id_daftar', $id)->get('daftar')->row_array();
		$data = array_merge(
				$this->db->where('nik', $daftar['username'])->join('kartu_keluarga', 'kartu_keluarga.no_kk=anggota.nkk')
					->get('anggota')
				->row_array(),
				$this->db->where('nama', $this->session->userdata('kantor'))
					->select('nama as kelurahan, alamat, telepon, lurah, kode_pos, kode_surat, nip')
					->get('kelurahan')
				->row_array()
			);
		$data['kelurahancase'] = ucfirst(strtolower($data['kelurahan']));
		$pelayanan = $this->db->where('id_pelayanan', $daftar['id_pelayanan'])->get('pelayanan')->row_array();
		$document = $phpWord->loadTemplate('./assets/file_surat/'.$pelayanan['nama'].'.docx');

		switch ($daftar['id_pelayanan']) {
			case 1:
				echo 'Surat Boro';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('surat_boro')->row_array());
				break;
			case 2:
				echo 'Surat Permohonan Kredit Bank';
				break;
			case 3:
				echo 'Surat Pindah Tempat Keluar Kabupaten';
				break;
			case 4:
				echo 'Surat SKTM';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('sktm')->row_array());
				break;
			case 5:
				echo 'Surat Keterangan Catatan Kepolisian';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('skck')->row_array());
				$data['pekerjaan_ortu'] = $this->db->where([
					'nkk' => $data['nkk'],
					'nama' => $data['nama']
				])->select('pddk as kerjaortu')->get('anggota')->row_array();
				break;
			case 6:
				echo 'Surat Keterangan Domisili';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('surat_domisili')->row_array());
				break;
			case 7:
				echo 'Ijin Mendirikan Bangunan';
				break;
			case 8:
				echo 'Ijin Usaha Mikro Kecil';
				break;
			case 9:
				echo 'Persyaratan Domisili Penduduk';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('sp_domisili')->row_array());
				break;
			case 10:
				echo 'Persyaratan Mencari Kartu Tanda Penduduk';
				break;
			case 11:
				echo 'Persyaratan Belum Nikah';
				// $data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('')->row_array());
				break;
			case 12:
				echo 'Persyaratan Nikah Atau Belum Nikah';
				break;
			case 13:
				echo 'Persyaratan Ahli Waris';
				break;
			case 14:
				echo 'Persyaratan Domisili Usaha';
				// -
				break;
			case 15:
				echo 'Persyaratan Kehilangan';
				$data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('sk_kehilangan')->row_array());
				break;
			case 16:
				echo 'Persyaratan Ijin Keramaian';
				// $data = array_merge($data, $this->db->where('NIK', $data['nik'])->order_by('0', 'desc')->get('sk_kehilangan')->row_array());
				break;
			case 17:
				echo 'Surat Keterangan Satu Nama';
				// -
				break;
		}
		$document->setValue('human_date', human_date(date('Y-m-d')));
		foreach ($data as $key => $row) {
			$document->setValue($key, $row);
		}
		$name = $pelayanan['nama'] .' '.human_date(date('Y-m-d')).' ['.date('H.i.s').'].docx';
		$document->saveAs($name);
		rename($name, "./assets/file_surat_unduh/{$name}");
		header('Location: ../../../assets/file_surat_unduh/'. $name);
	}
}
