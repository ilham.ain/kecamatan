<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function get_pelayanan() {
		$result = $this->db->where([
			'desa' => $this->session->userdata('kantor'),
			'posisi' => 'pendaftar',
			'status' => 'belum diverifikasi',
		])->get('daftar')->num_rows();
		print_r($result);
	}
}
