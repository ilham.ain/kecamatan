<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$result = $this->db->get('berita')->result_array();
		$this->load->view('admin/berita/index', array('result'=> $result));
	}

	public function insert() {
		$config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['encrypt_name']			= true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            print_r($this->upload->display_errors());
        } else  {
        	$this->db->insert('berita', [
            	'judul' => $this->input->post('judul'),
            	'isi' => $this->input->post('isi'),
            	'gambar' => 'assets/img/'. $this->upload->data('file_name'),
            	'tgl' => date('Y-m-d')
            ]);
            redirect('admin/berita');
    	}
	}

    public function edit($id) {
        $result = $this->db->where('id_berita', $id)->get('berita')->row_array();
        if($result) {
            $this->load->view('admin/berita/edit', array('result' => $result));
        } else {
            redirect('admin/berita');
        }
    }

    public function update($id) {
        if($_FILES['gambar']['size'] > 0) {
            $config['upload_path']          = './assets/img/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 2000;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar')) {
                print_r($this->upload->display_errors());
            } else  {
                $this->db->set('gambar', 'assets/img/'. $this->upload->data('file_name'));
            }
        }
        $this->db->set([
            'isi' => $this->input->post('isi'),
            'judul' => $this->input->post('judul')
        ])->where('id_berita', $id)->update('berita');
        redirect('admin/berita');
    }
}
