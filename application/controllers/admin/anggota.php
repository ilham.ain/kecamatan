<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('role')) {
			redirect('admin');
		}
	}

	public function index() {
		$this->load->view('admin/anggota/index');
	}

	public function get() {
		$result = $this->db->select('nik, desa, nkk, nama, kelamin, tempat_lahir, tanggal_lahir, agama, status_nikah')
			->where('desa', $this->session->userdata('kantor'))
			->get('anggota')
		->result_array();
		print_r(json_encode($result));
	}
}
