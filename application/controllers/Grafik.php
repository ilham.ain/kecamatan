<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

	public function index() {
		$result = $this->db->get('grafik_judul')->result_array();
		$this->load->view('grafik', array('result' => $result));
	}

	public function get($id) {
		$result = $this->db->join('grafik_subjudul', 'grafik_subjudul.id_subjudul=grafik_isi.id_subjudul')
			->join('grafik_judul', 'grafik_judul.id_judul=grafik_subjudul.id_judul')
			->where('grafik_judul.id_judul', $id)
		->get('grafik_isi')->result_array();
		$result_kelurahan = $this->db->order_by('kode', 'asc')->get('grafik_kelurahan')->result_array();
		foreach ($result_kelurahan as $row) {
			$data['data']['labels'][] = $row['kode'] . ' - ' . $row['kelurahan'];
		}

		foreach ($result as $index => $row) {
			$dump_data[$row['id_subjudul']]['label'] = $row['subjudul'];
			$dump_data[$row['id_subjudul']]['backgroundColor'] = $row['warna'];
			$dump_data[$row['id_subjudul']]['borderColor'] = $row['warna'];
			$dump_data[$row['id_subjudul']]['data'][] = $row['isi'];
			$dump_data[$row['id_subjudul']]['fill'] = 'false';
		}

		foreach ($dump_data as $key => $row) {
			$data['data']['datasets'][] = $row;
		}

		$data['text'] = $result[0]['judul'];

		print_r(json_encode($data));
	}

	public function daftar() {
		print_r(json_encode($this->db->get('grafik_judul')->result_array()));
	}
}
