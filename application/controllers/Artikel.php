<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Artikel Controller
*/
class Artikel extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Myartikel');
	}

	public function index()
	{
		
		$data = array('admin' => $this->Myartikel->get_artikel());
	
		$this->load->view('basic/menu');
		$this->load->view('artikel/view', $data);
		$this->load->view('basic/footer');
	}

	public function view_artikel(){
		$this->load->model('Myartikel');
		$data['all_artikel'] = $this->Myartikel->find_artikel($this->input->get('id_artikel'));
	
		$this->load->view('basic/menu');
		$this->load->view('artikel/lengkap', $data);
		$this->load->view('basic/footer');
	}

	}

?>