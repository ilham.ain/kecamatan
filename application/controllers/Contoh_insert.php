<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contoh_insert extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}

	public function contoh_insert($parent_id)
	{	
		$this->db->insert('pengaduan',[
			'parent_id' => $parent_id,
			'nama' => 'Lorem Ipsum',
			'komentar' => 'Lorem Ipsum Dolor Sit Amet'
		]);
	}
}