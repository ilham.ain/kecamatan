<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Pelayan Khusus Controller
*/
class Pelayanan_Khusus extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$this->load->view('basic/menu');
		$this->load->view('pelayanan/khusus');
		$this->load->view('basic/footer');
	}
}
?>