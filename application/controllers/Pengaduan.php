<?php defined('BASEPATH') OR exit ('No direct script access allowed');

/**
* Pengaduan Controller
*/
class Pengaduan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mypengaduan');
		
        $this->load->helper('form','url');
	}

	public function index(){
		$data['pengaduan'] = $this->Mypengaduan->read();

		$data['komentar'] = $this->Mypengaduan->komentar();

		$this->load->view('basic/menu');
		$this->load->view('konten/pengaduan', $data);
		$this->load->view('basic/footer');
	}

	public function create(){

		$this->load->view('basic/menu');
		
		$this->load->view('basic/footer');
	}
}
?>