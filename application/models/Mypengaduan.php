<?php 

/**
* Pengaduan
*/
class Mypengaduan extends CI_Model
{
	function komentar() {
		
		$sql = $this->db->query("SELECT * FROM pengaduan WHERE parent_id ORDER BY id ASC");
		return $sql->result_array();	
		
		if($sql->num_rows > 0){
			$i = 0;
			$list = '';
			while($row = $result->fetch_array()){
				$list[$i] = $row;
				$i++;
			}
			return $list;
		}
	}

	public function read()
    {
            $data = $this->db->query("SELECT * FROM pengaduan where parent_id IS NULL ORDER BY id DESC");
            return $data->result_array();
    }

    function input_data($data,$table){
		$this->db->insert($table,$data);
	}
     
    public function validation($mode){
      	$this->load->library('form_validation'); 
      	if($mode == "save"){
      		$this->form_validation->set_rules('tanggal_pengaduan', 'tanggal_pengaduan', 'required|date(d-m-Y)');
            $this->form_validation->set_rules('nama', 'nama', 'required|max_length[50]');    
            $this->form_validation->set_rules('komentar', 'Jenis komentar', 'required');    
            $this->form_validation->set_rules('email', 'email', 'required|email');    
            $this->form_validation->set_rules('alamat', 'alamat', 'required');        
            if($this->form_validation->run()) { 
            	return TRUE; 
            }else{
                return FALSE;  
            }
        }
    }


    public function save(){
	    $data = array(
	      "tanggal_pengaduan" => $this->input->post('tanggal_pengaduan'),
	      "nama" => $this->input->post('nama'),
	      "komentar" => $this->input->post('komentar'),
	      "email" => $this->input->post('email'),
	      "kepada" => $this->input->post('kepada'),
	      "alamat" => $this->input->post('alamat')
	    );
	    
	    $this->db->insert('siswa', $data); // Untuk mengeksekusi perintah insert data
	  }
  
}