<section class="contact" style="padding-top: 70px;">
  <div class="container">
    <header class="section-header wow fadeInUp">
          <h3>FORM PENDAFTARAN</h3>
        </header>
      <div class="box">
        <div class="modal-content">
          <form method="post" action="<?= base_url()?>daftar/tambah" enctype="multipart/form-data" class="form-horizontal">
            <div class="modal-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">NIK</label>
                <div class="col-sm-8">
                  <input name="nik" class="form-control" placeholder="Nomor Induk Kewarganegaraan" required type="number">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">NKK</label>
                <div class="col-sm-8">
                  <input name="nkk" class="form-control" placeholder="Nomor Kartu Keluarga" required type="number">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Desa</label>
                <div class="col-sm-8"> 
                  <select required name="desa" class="form-control select2">
                    <option></option>
                    <option value="BAGO">Bago</option>
                    <option value="BOTORAN">Botoran</option>
                    <option value="JEPUN">Jepun</option>
                    <option value="KAMPUNGDALEM">Kampungdalem</option>
                    <option value="KARANGWARU">Karangwaru</option>
                    <option value="KAUMAN">Kauman</option>
                    <option value="KEDUNGSOKO">Kedungsoko</option>
                    <option value="KENAYAN">Kenayan</option>
                    <option value="KEPATIHAN">Kepatihan</option>
                    <option value="KUTOANYAR">Kutoanyar</option>
                    <option value="PANGGUNGREJO">Panggungrejo</option>
                    <option value="SEMBUNG">Sembung</option>
                    <option value="TAMANAN">Tamanan</option>
                    <option value="TERTEK">Tertek</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama</label>
                <div class="col-sm-8">
                  <input name="nama" class="form-control" placeholder="Nama Lengkap" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Jenis Kelamin</label>
                <div class="col-sm-8">
                  <select required name="kelamin" class="form-control select2">
                    <option></option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Tempat Lahir</label>
                <div class="col-sm-8">
                  <input name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Tanggal Lahir</label>
                <div class="col-sm-8">
                  <input name="tanggal_lahir" class="form-control datepick" placeholder="Tanggal Lahir" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Golongan Darah</label>
                <div class="col-sm-8">
                  <select required name="gol_darah" class="select2 form-control">
                    <option></option>
                    <option value='A'>A</option>
                    <option value='B'>B</option>
                    <option value='AB'>AB</option>
                    <option value='O'>O</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Agama</label>
                <div class="col-sm-8">
                  <select required name="agama" class="select2 form-control">
                    <option></option>
                    <option value='Buddha'>Buddha</option>
                    <option value='Hindu'>Hindu</option>
                    <option value='Islam'>Islam</option>
                    <option value='Konghucu'>Konghucu</option>
                    <option value='Kristen Katolik'>Kristen Katolik</option>
                    <option value='Kristen Protestan'>Kristen Protestan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status Nikah</label>
                <div class="col-sm-8">
                  <select name="status_nikah" required class="select2 form-control">
                    <option></option>
                    <option value="Cerai Hidup">Cerai Hidup</option>
                    <option value="Cerai Mati">Cerai Mati</option>
                    <option value="Kawin">Kawin</option>
                    <option value="Belum Kawin">Belum Kawin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">SHDRT</label>
                <div class="col-sm-8">
                  <select required name="shdrt" class="select2 form-control">
                    <option></option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Istri">Istri</option>
                    <option value="Anak">Anak</option>
                    <option value="Cucu">Cucu</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Pendidikan Terakhir</label>
                <div class="col-sm-8">
                  <select required name="pddk" class="select2 form-control" placeholder="Pendidikan Terakhir">
                    <option></option>
                    <option value="SD">SD</option>
                    <option value="SMP/MTS">SMP/MTS</option>
                    <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Pekerjaan</label>
                <div class="col-sm-8">
                  <input name="pekerjaan" class="form-control" placeholder="Pekerjaan" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Kebangsaan</label>
                <div class="col-sm-8">
                  <input name="kebangsaan" class="form-control" placeholder="Kebangsaan" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Ibu</label>
                <div class="col-sm-8">
                  <input name="ibu" class="form-control" placeholder="Nama Ibu" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Ayah</label>
                <div class="col-sm-8">
                  <input name="ayah" class="form-control" placeholder="Nama Ayah" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Alamat</label>
                <div class="col-sm-8">
                  <textarea name="alamat" class="form-control" placeholder="Alamat" required type="text"> </textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="tipe" value="new">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-danger">Daftar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</section>