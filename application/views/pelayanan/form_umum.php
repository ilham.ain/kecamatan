<section id="services">
  <div class="container" >
    <header class="section-header wow fadeInUp">
      <h3>Daftar Pelayanan</h3>
    </header>
    
    <input type="hidden" name="id_pelayanan" readonly value="<?= $find['id_pelayanan'] ?>">
  
    <?php if(empty($_SESSION['username'])){ ?>
    <br>
    <div class="row">
      <div class="col-md-4">
        <img src="<?= base_url() ?><?= $find['gambar'] ?>" width="100%">
      </div>
      <div class="col-md-8">
        <b>Pelayanan :&nbsp;&nbsp;</b><?= ucwords($find['nama']) ?>
        <p><?= ucwords($find['sinopsis']) ?>
      </div>
    </div>
    <br>
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Alert!</h4>
        Untuk memperoleh pelayanan anda harus mendaftarkan diri , kemudian baru login untuk mendapatkan jenis pelayanan yang diinginkan !!
    </div>  
  <?php } ?>
  
  <?php if(!empty($_SESSION['username'])){ ?>
    <h2 class="page-header">Daftar Pelayan</h2>
    <div class="row">
        <div class="col-md-4">
            <img src="<?= base_url() ?><?= $find['gambar'] ?>" width="100%">
        </div>
        <div class="col-md-8">
            <b>Pelayanan :&nbsp;&nbsp;</b><?= ucwords($find['nama']) ?>
            <p><?= ucwords($find['sinopsis']) ?>
        </div>
    </div>
    <br>

  <?php if($find['id_pelayanan']==1) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Boro</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly placeholder="Kode Pelayanan" value="<?= $id_pelayanan?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Desa</label>
          <div class="col-sm-10">
            <input name="desa" class="form-control" readonly placeholder="Desa" value="<?= $_SESSION['desa'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Tujuan</label>
          <div class="col-sm-10">
            <input name="tujuan" class="form-control" placeholder="Tujuan">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Tanggal Berangkat</label>
          <div class="col-sm-10">
            <input name="tgl_berangkat" class="form-control datepick" placeholder="Tanggal Berangkat">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Sampai Tanggal</label>
          <div class="col-sm-10">
            <input name="sampai_tgl" class="form-control datepick" placeholder="Sampai Tanggal">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan</label>
          <div class="col-sm-10">
            <input name="keperluan" class="form-control" placeholder="Keperluan">
          </div>
        </div>
        <span id="pengikut"></span>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="button" onclick="tambah()" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Pengikut</button>
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
        <script>var x=0;</script>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==2) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Permohonan Kredit Bank</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $find['id_pelayanan'] ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $find['id_pelayanan']?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota']?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Desa</label>
          <div class="col-sm-10">
            <input name="desa" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['desa'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-10">
            <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Alamat</label>
          <div class="col-sm-10">
            <textarea name="alamat" class="form-control" placeholder="alamat"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan</label>
          <div class="col-sm-10">
            <textarea name="keperluan" class="form-control" placeholder="Keperluan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==3) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Pindah Rumah</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly placeholder="Kode Pelayanan" value="<?= $id_pelayanan?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly placeholder="Kode Pelayanan" value="<?= $_SESSION['id_anggota'] ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Desa</label>
          <div class="col-sm-10">
            <input name="desa" class="form-control" readonly placeholder="Desa" value="<?= $_SESSION['desa'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Tujuan</label>
          <div class="col-sm-10">
            <input name="tujuan" class="form-control" placeholder="Tujuan">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Tanggal Berangkat</label>
          <div class="col-sm-10">
            <input name="tgl_berangkat" class="form-control datepick" placeholder="Tanggal Berangkat">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan</label>
          <div class="col-sm-10">
            <input name="keperluan" class="form-control" placeholder="Keperluan">
          </div>
        </div>
        <span id="pengikut"></span>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="button" onclick="tambah()" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Pengikut</button>
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
        <script>var x=0;</script>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==4) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Tidak mampu</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $id_pelayanan ?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota'] ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Kegunaan</label>
          <div class="col-sm-10">
            <textarea name="kegunaan" class="form-control" placeholder="Kegunaan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==5) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Catatan Kepolisian </h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $id_pelayanan?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota'] ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan </label>
          <div class="col-sm-10">
            <input name="keperluan" class="form-control" placeholder="keperluan">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==6) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Domisili</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $id_pelayanan?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota']  ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-10">
            <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan</label>
          <div class="col-sm-10">
            <textarea name="keperluan" class="form-control" placeholder="Keperluan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==7) { ?>
    <h2 class="page-header">Pengisian Data untuk Ijin Mendirikan Bangunan</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $id_pelayanan?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota']  ?>">
        <input type="hidden" name="desa" class="form-control" readonly value="<?= $_SESSION['desa']  ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Tanggal Lahir</label>
          <div class="col-sm-10">
            <input name="tgl_lahir" class="form-control datepick">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Pekerjaan</label>
          <div class="col-sm-10">
            <input name="pekerjaan" class="form-control" placeholder="Keterangan">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Alamat</label>
          <div class="col-sm-10">
            <textarea name="alamat" class="form-control" placeholder="Keperluan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } elseif ($find['id_pelayanan']==8) { ?>
    <h2 class="page-header">Pengisian Data untuk Surat Keterangan Usaha Mikro Kecil</h2>
    <div class="row">
      <form class="form-horizontal" method="post" action="controller/daftar/create.php?id_pelayanan=<?= $id_pelayanan ?>">
        <input type="hidden" name="id_pelayanan" class="form-control" readonly value="<?= $id_pelayanan?>">
        <input type="hidden" name="id_anggota" class="form-control" readonly value="<?= $_SESSION['id_anggota']  ?>">
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input name="username" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['username'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Desa</label>
          <div class="col-sm-10">
            <input name="desa" class="form-control" readonly placeholder="Username" value="<?= $_SESSION['desa'] ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-10">
            <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Keperluan</label>
          <div class="col-sm-10">
            <textarea name="keperluan" class="form-control" placeholder="Keperluan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Daftar</button>
          </div>
        </div>
      </form>
    </div>
    <?php } ?>
<?php } ?>
   </p>
   </div>
 </div>
</p>
</div>
</div>
</section>
</div>
</div></section>
