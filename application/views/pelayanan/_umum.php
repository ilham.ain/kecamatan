<section id="portfolio" style="padding-top: 70px;">
    <div class="container">
      <div class="section-header">
          <h3>Pelayanan Umum</h3>
          <p>Silahkan pilih pelayanan umum yang ingin anda ajukan. Permintaan pelayanan Anda akan segera di tanggapi oleh pihak terkait.</p>
        </div>
        <section class="content">
                <div class="row">
                <?php 
                       foreach($umum as $res) { 
                        $gambar = str_replace('../../../', '', $res['gambar']);
                     ?>
                    <div class="gambar jarak_atas col-md-3">
                        <a href="<?= base_url('user/p_form') ?>?id_pelayanan=<?= $res['id_pelayanan'] ?>">
                            <img width="100%" style="padding-bottom: 20px" src="<?= base_url()?><?= $gambar ?>" />
                        </a>
                    </div>
                    
                    <?php } ?>
                    <div class="clear"></div>
                </div>
        </section>
    </div>
</section>