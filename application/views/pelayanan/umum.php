<section id="portfolio">
    <div class="container">
      <div class="section-header">
          <h3>Pelayanan Umum</h3>
          <p>Silahkan pilih pelayanan umum yang ingin anda ajukan. Permintaan pelayanan Anda akan segera di tanggapi oleh pihak terkait.</p>
        </div>
        <section class="content">
                <div class="row">
                <?php 
                        foreach($umum as $res) { 
                        $gambar = str_replace('../../../', '', $res['gambar']);
                     ?>
                    <div class="gambar jarak_atas col-md-3">
                        <a href="pelayanan/form?id_pelayanan=<?= $res['id_pelayanan'] ?>">
                            <img width="100%" style="padding-bottom: 20px" src="<?= $gambar ?>" />
                        </a>
                    </div>
                    
                    <?php } ?>
                    <div class="clear"></div>
                </div>
        </section>
    </div>
</section>