 <div class="pull-left">
                  <a href="#login" role="button" data-toogle="modal">
                    <span class="btn btn-flat btn-danger">Login</span>
                  </a>
                </div>
  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">

      <div class="container container-wrapper"> 
      <header class="section-header"><h3>Berita</h3></header>

       <div class="row about-cols">
      <?php foreach ($all_berita as $news) { ?>

          <div class="col-md-6 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="<?php echo base_url(); ?><?= $news['gambar'] ?>" width="100%" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#"><?= $news['judul'] ?></a></h2>
              <p>
                <?= substr($news['isi'], 0,200) ?>.........
                 <a href="berita/lengkap?id_berita=<?= $news['id_berita'] ?>" class="btn btn-info btn-xs">Selengkapnya</a>
              </p>
            </div>
          </div>
      <?php } ?>

        </div>
      </div>

      <div class="container">
        <header class="section-header"><h3>Artikel</h3></header>
        <div class="row about-cols">
          
        <?php foreach ($admin as $key) { ?>
          <div class="col-md-6 wow fadeInUp">
            <div class="about-col">
              <div class="img"  width="450px" style="text-align: center;">
                <img src="<?php echo base_url(); ?><?= $key['gambar'] ?>" width="450px" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#"><?= $key['judul']?></a></h2>
              <p>
                <?= substr($key['isi'], 0,200) ?>.........
                 <a href="artikel/view_artikel?id_artikel=<?= $key['id_artikel'] ?>" class="btn btn-info btn-xs">Selengkapnya</a>
              </p>
            </div>
          </div>
       <?php } ?>
      </div>
    </div>
    </section><!-- #about -->

    
    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Team</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div> 

        <div class="row">

          <div class="col-lg-3 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="<?php echo base_url(); ?>assets/bizpage/img/team-1.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Walter White</h4>
                  <span>Chief Executive Officer</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="<?php echo base_url(); ?>assets/bizpage/img/team-2.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Sarah Jhonson</h4>
                  <span>Product Manager</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="<?php echo base_url(); ?>assets/bizpage/img/team-3.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>William Anderson</h4>
                  <span>CTO</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div class="member">
              <img src="<?php echo base_url(); ?>assets/bizpage/img/team-4.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Amanda Jepson</h4>
                  <span>Accountant</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #team -->

  </main>
