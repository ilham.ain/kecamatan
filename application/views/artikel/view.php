  <!--<main id="main">
    <section>
    <div class="container">
        <div class="box"><?php foreach($admin as $a) {?>
        	<?php echo $a['judul']; ?><br>

        	<?php } ?>
        </div>
    </div>
    </section>
  </main>
-->
    <!--==========================
      Artikel Us Section
    ============================-->
    <section id="artikel">
      <div class="container">

        <header class="section-header">
          <h3>Artikel Kecamatan Tulungagung</h3>
          <p>Berisi beberapa artikel terkait di wilayah Kecamatan Tulungagung. Jika Anda ingin memberikan artikel sekitar Kabupaten Tulungagung Dapat mengirim print out artikel beserta bukti berupa foto. Kirim ke Kecamatan atau dapat melalui kelurahan setempat.</p>
        </header>
         
        <div class="row artikel-cols">
        <?php foreach ($admin as $data) {?>

          <div class="col-md-4 wow fadeInUp">
            <div class="artikel-col">
              <div class="img">
                <img src="<?php echo base_url(); ?><?= $data['gambar'] ?>" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#"><?= $data['judul'] ?> </a></h2>
              <p>
                <?= substr($data['isi'], 0,200) ?>.........
                 <a href="artikel/view_artikel?id_artikel=<?= $data['id_artikel'] ?>" class="btn btn-info btn-xs">Selengkapnya</a>
              </p>
            </div>

          </div>
        <?php } ?>
      </div>
    </div>
    </section><!-- #artikel -->