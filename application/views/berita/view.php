<!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Berita Kecamatan Tulungagung</h3>
          <p>Berisi beberapa berita di wilayah Kecamatan Tulungagung. Jika Anda ingin memberikan berita sekitar Kabupaten Tulungagung Dapat mengirim print out berita beserta bukti berupa foto. Kirim ke Kecamatan atau dapat melalui kelurahan setempat.</p>
        </header>
        <div class="row about-cols">
          
        <?php foreach($all_berita as $data) {  ?>

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="<?php echo base_url(); ?><?= $data['gambar'] ?>" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#"><?= $data['judul'] ?> </a></h2>
              <p>
                <?= substr($data['isi'], 0,200) ?>.........
                 <a href="berita/lengkap?id_berita=<?= $data['id_berita'] ?>" class="btn btn-info btn-xs">Selengkapnya</a>
              </p>
            </div>
          </div>
      <?php } ?>

        </div>
      </div>
    </section><!-- #about -->