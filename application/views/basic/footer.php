<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Kecamatan Tulungagung 2018</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Website <a href="https://bootstrapmade.com/">P A T E N</a> by Anonymous
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/lightbox/js/lightbox.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bizpage/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url(); ?>assets/bizpage/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url(); ?>assets/bizpage/js/main.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/fastclick/fastclick.js"></script>
  <script src="<?php echo base_url(); ?>assets/AdminLTE/dist/js/app.min.js"></script>

  <script>
    function login()
    {
      $('.login').modal('show');
    }
    
    function register()
    {
      $('.register').modal('show');
    }
    function kotak{
    $('.responsive-tabs').responsiveTabs({
      accordionOn: ['xs', 'sm'] // xs, sm, md, lg 
    });
  }
    
</script>
</body>
</html>
