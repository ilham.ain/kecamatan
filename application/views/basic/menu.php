<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Kecamatan
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>assets/bizpage/img/logo.png" rel="icon">
  <link href="<?php echo base_url(); ?>assets/bizpage/img/logo.png" rel="logo">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/bizpage/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" >
  <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/iCheck/square/blue.css">

  <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/select2/select2.min.css" rel="stylesheet" />


  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url(); ?>assets/bizpage/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/adminlte/plugins/datepicker/datepicker3.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/adminlte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/datepicker/datepicker3.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/select2/select2.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/textillate/assets/animate.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" >
  <link href="<?php echo base_url(); ?>assets/AdminLTE/dist/css/skins/_all-skins.min.css" rel="stylesheet" >
 </head>

<body  class="skin-red layout-top-nav" style="height: auto;">

  <!--==========================
    Header
  ============================-->
  <header id="header" style="height: 70px;">
  <div class="navbar navbar-default navbar-fixed-top" style="background-color: black; height: 70px;">
    <div class="container-fluid">
      <div id="logo" class="pull-left">
        <h1><a href="<?php echo base_url(); ?>" class="scrollto">PATEN</a></h1>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo base_url(); ?>">Beranda</a></li>
          <li><a href="<?php echo base_url(); ?>berita">Berita</a></li>
          <li><a href="<?php echo base_url(); ?>artikel">Artikel</a></li>
          <li><a href="<?php echo base_url(); ?>Panduan">Panduan</a></li>
          <li><a href="<?php echo base_url(); ?>Pengaduan">Pengaduan</a></li>
          <li class="menu-has-children"><a href="">Pelayanan</a>
            <ul>
              <li><a href="<?php echo base_url(); ?>pelayanan_khusus">Pelayanan Khusus</a></li>
              <li><a href="<?php echo base_url(); ?>pelayanan">Pelayanan Umum</a></li>
              <li><a href="<?php echo base_url(); ?>Tempat">Tempat/Waktu Pelayanan</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url(); ?>Sop">SOP</a></li>
          <li><a href="<?php echo base_url(); ?>Grafik">Grafik</a></li>
          <li class="menu-has-children"><a href="#">Masuk</a>
            <ul>
              <li><a href="<?= base_url('user') ?>">Login</a></li>
              <li><a href="<?= base_url('daftar') ?>">Daftar</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
      <!-- #nav-menu-container -->
  </div><!-- #header -->
</header>


   <!--==========================
    POP UP LOGIN
  ============================-->
  <div id="login" class="modal hide fade in" role="dialog">
   <div class="modal modal-default fade login">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Login</h4>
          </div>
          <form method="post" action="<?php echo base_url(); ?>akun/login">
            <div class="modal-body">
              <div class="form-group has-feedback">
                <input type="text" name="username" placeholder="Username" required="required" class="form-control">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" name="password" placeholder="Password" required="required" class="form-control">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="tipe" value="new">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-success">Masuk</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

    

