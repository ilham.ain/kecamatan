<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Terpadu | Login
  </title>
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="icon">
  <link href="<?= base_url('assets/bizpage/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/css/style.css') ?>" rel="stylesheet">
</head>

<body>
  <main id="main">
    <section id="about" style="min-height: 600px">
      <div class="container container-wrapper"> 
        <header class="section-header"><h3>Login</h3></header>
        <div class="row about-cols">
          <div class="container">
            <div class="offset-md-3 col-md-6">
              <?php if($this->session->flashdata('login')) { ?>
              <div class="alert alert-danger" role="alert">
                <strong>Username Password Salah!</strong><br>Mohon Periksa Username dan Password anda.  
              </div>
              <?php } ?>
              <form action="<?= base_url('login/login') ?>" method="post">
                <div class="form-group">
                  <input type="text" name="username" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button class="btn btn-info btn-block">Login</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Kecamatan Tulungagung 2018</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Website <a href="#">PATEN</a> by D-Junsoft
      </div>
    </div>
  </footer>
  <script src="<?= base_url('assets/bizpage/lib/jquery/jquery.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
</script>
</body>
</html>
