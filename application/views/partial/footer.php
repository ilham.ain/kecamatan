  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Kecamatan Tulungagung 2018</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Website <a href="https://bootstrapmade.com/">P A T E N</a> by Anonymous
      </div>
    </div>
  </footer>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <script src="<?php echo base_url('assets/bizpage/lib/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/jquery/jquery-migrate.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/easing/easing.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/superfish/hoverIntent.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/superfish/superfish.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/wow/wow.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/waypoints/waypoints.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/counterup/counterup.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/owlcarousel/owl.carousel.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/isotope/isotope.pkgd.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/lightbox/js/lightbox.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/lib/touchSwipe/jquery.touchSwipe.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/contactform/contactform.js') ?>"></script>
  <script src="<?php echo base_url('assets/bizpage/js/main.js') ?>"></script>
  <?php if(!isset($result)) { ?>
  <script>
    $(document).ready(function() {
      $('#header').addClass('header-scrolled');
      $(window).unbind('scroll');
      $('#about').css('padding-top', '90px');
    });
  </script>
  <?php } ?>
  <script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
</body>
</html>
