<?php defined('BASEPATH') OR exit('No direct script access allowed')?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Kecamatan
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="icon">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="logo">
  <link href="<?= base_url('assets/bizpage/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/animate/animate.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/lightbox/css/lightbox.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/AdminLTE/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" >
  <link href="<?= base_url('assets/AdminLTE/plugins/iCheck/square/blue.css') ?>">
  <link href="<?= base_url('assets/AdminLTE/plugins/select2/select2.min.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/bizpage/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/adminlte/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/adminlte/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" />
</head>
<body>
  <div id="header" class="navbar navbar-default navbar-fixed-top" style="background-color: black; height: 70px;">
    <div class="container-fluid">
      <div id="logo" class="pull-left">
        <h1><a href="<?= base_url() ?>" class="scrollto">PATEN</a></h1>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="<?= base_url()?>home">Beranda</a></li>
          <li class="menu-has-children"><a href="#" >Pelayanan</a>
            <ul>
              <li><a href="<?= base_url()?>pelayanan_/pelayanan_khusus">Pelayanan Khusus</a></li>
              <li><a href="<?= base_url('user/pelayanan')?>">Pelayanan Umum</a></li>
              <li><a href="<?= base_url('user/tempat')?>">Tempat/Waktu Pelayanan</a></li>
            </ul> 
          </li>
          <li><a href="<?= base_url() ?>user/berita_/read">Berita</a></li>
          <li><a href="<?= base_url() ?>user/artikel_/read">Artikel</a></li>
          <li><a href="<?= base_url() ?>user/konten_/panduan">Panduan</a></li>
          <li><a href="<?= base_url() ?>user/konten_/pengaduan">Pengaduan</a></li>
          <li><a href="<?= base_url() ?>user/konten_/sop">SOP</a></li>
          <li><a href="<?= base_url() ?>user/konten_/status">Status Pendaftaran</a></li>
          <li class="menu-has-children"><a href="#"><?= $this->session->userdata('nama') ?></a>
            <ul>
              <li><a href="#">Pengaturan</a></li>
              <li><a href="<?= base_url('user/logout') ?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>
