<?php defined('BASEPATH') OR exit('No direct script access allowed')?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Kecamatan
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="icon">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="logo">
  <link href="<?= base_url('assets/bizpage/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/animate/animate.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/lightbox/css/lightbox.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/AdminLTE/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" >
  <link href="<?= base_url('assets/AdminLTE/plugins/iCheck/square/blue.css') ?>">
  <link href="<?= base_url('assets/AdminLTE/plugins/select2/select2.min.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/bizpage/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/adminlte/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/adminlte/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" />
</head>
<body>
  <header id="header">
    <div class="container-fluid">
      <div id="logo" class="pull-left">
        <h1><a href="<?= base_url() ?>" class="scrollto">PATEN</a></h1>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="<?= base_url()?>home">Beranda</a></li>
          <li class="menu-has-children"><a href="#" >Pelayanan</a>
            <ul>
              <li><a href="<?= base_url()?>pelayanan_/pelayanan_khusus">Pelayanan Khusus</a></li>
              <li><a href="<?= base_url('user/pelayanan')?>">Pelayanan Umum</a></li>
              <li><a href="<?= base_url('user/tempat')?>">Tempat/Waktu Pelayanan</a></li>
            </ul> 
          </li>
          <li><a href="<?= base_url() ?>user/berita_/read">Berita</a></li>
          <li><a href="<?= base_url() ?>user/artikel_/read">Artikel</a></li>
          <li><a href="<?= base_url() ?>user/konten_/panduan">Panduan</a></li>
          <li><a href="<?= base_url() ?>user/konten_/pengaduan">Pengaduan</a></li>
          <li><a href="<?= base_url() ?>user/konten_/sop">SOP</a></li>
          <li><a href="<?= base_url() ?>user/konten_/status">Status Pendaftaran</a></li>
          <li class="menu-has-children"><a href="#"><?= $this->session->userdata('nama') ?></a>
            <ul>
              <li><a href="#">Pengaturan</a></li>
              <li><a href="<?= base_url('user/logout') ?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </header>

   <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>assets/gambar/1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Penghargaan Kota Tulungagung</h2>
                <p>Baru - baru ini Kota Tulungagung mendapatkan Penghargaan dari Gubernur Jawa Timur. Kota Tulungagung menjadi kota percontohan mulai dari program penghijauan, Kemajuan teknologi dan pembangunan sekolah nelayan</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/gambar/2.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Program Perempuan Mandiri</h2>
                <p>Program - program dari Kota tulungagung menjadikan usaha mandiri bagi perempuan tulungagung</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/gambar/3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Pantai Popoh</h2>
                <p>Pantai merupakan salah satu dari sekian banyak destinasi wisata di Kabupaten Tulungagung yang tersebar di wilayah selatan Kabupaten Tulungagung. Ada lebih dari 10 pantai yang terkenal termasuk Pantai Popoh dan beberapa lainnya yang belum ter ekspos.</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->



 
  