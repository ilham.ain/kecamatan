<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Kecamatan
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>../assets/bizpage/img/logo.png" rel="icon">
  <link href="<?php echo base_url(); ?>../assets/bizpage/img/logo.png" rel="logo">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/bizpage/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" >
  <link href="<?php echo base_url(); ?>../assets/AdminLTE/plugins/iCheck/square/blue.css">

  <link href="<?php echo base_url(); ?>../assets/AdminLTE/plugins/select2/select2.min.css" rel="stylesheet" />


  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url(); ?>../assets/bizpage/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>../assets/adminlte/plugins/datepicker/datepicker3.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>../assets/adminlte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
 </head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="<?php echo base_url(); ?>../" class="scrollto">PATEN</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo base_url(); ?>../">Beranda</a></li>
          <li><a href="<?php echo base_url(); ?>../berita/Read">Berita</a></li>
          <li><a href="<?php echo base_url(); ?>../artikel/Read">Artikel</a></li>
          <li><a href="<?php echo base_url(); ?>../konten/Panduan">Panduan</a></li>
          <li><a href="<?php echo base_url(); ?>../konten/Pengaduan">Pengaduan</a></li>
          <li class="menu-has-children"><a href="">Pelayanan</a>
            <ul>
              <li><a href="<?php echo base_url(); ?>../pelayanan/Khusus">Pelayanan Khusus</a></li>
              <li><a href="<?php echo base_url(); ?>../pelayanan/Pelayanan">Pelayanan Umum</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url(); ?>../konten/Sop">SOP</a></li>
          <li><a href="<?php echo base_url(); ?>../konten/Tempat">Tempat/Waktu Pelayanan</a></li>
         <li class="menu-has-children"><a href="#">Masuk</a>
            <ul>
              <li class="user-footer">
                <div class="pull-left">
                  <button onclick="login()" class="btn btn-default btn-flat"><a>Masuk</a></button>
                </div>
                <div class="pull-right">
                  <button onclick="register()" href="register.php" class="btn btn-default btn-flat"><a>Daftar</a></button>
                </div>
              </li>
            </ul>
          </li>
            </ul>
          </div>
          
         </ul>
              
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

   <!--==========================
    POP UP LOGIN
  ============================-->

   <div class="modal modal-default fade login">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Login</h4>
          </div>
          <form method="post" action="<?php echo base_url(); ?>../akun/login">
            <div class="modal-body">
              <div class="form-group has-feedback">
                <input type="text" name="username" placeholder="Username" required="required" class="form-control">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" name="password" placeholder="Password" required="required" class="form-control">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="tipe" value="new">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-success">Masuk</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal modal-default fade register">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Daftar</h4>
          </div>
          <form method="post" action="controller/anggota/create.php" enctype="multipart/form-data" class="form-horizontal">
            <div class="modal-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">NIK</label>
                <div class="col-sm-8">
                  <input name="nik" class="form-control" placeholder="Nomor Induk Kewarganegaraan" required type="number">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">NKK</label>
                <div class="col-sm-8">
                  <input name="nkk" class="form-control" placeholder="Nomor Kartu Keluarga" required type="number">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Desa</label>
                <div class="col-sm-8"> 
                  <select required name="desa" class="form-control select2">
                    <option></option>
                    <option value="BAGO">Bago</option>
                    <option value="BOTORAN">Botoran</option>
                    <option value="JEPUN">Jepun</option>
                    <option value="KAMPUNGDALEM">Kampungdalem</option>
                    <option value="KARANGWARU">Karangwaru</option>
                    <option value="KAUMAN">Kauman</option>
                    <option value="KEDUNGSOKO">Kedungsoko</option>
                    <option value="KENAYAN">Kenayan</option>
                    <option value="KEPATIHAN">Kepatihan</option>
                    <option value="KUTOANYAR">Kutoanyar</option>
                    <option value="PANGGUNGREJO">Panggungrejo</option>
                    <option value="SEMBUNG">Sembung</option>
                    <option value="TAMANAN">Tamanan</option>
                    <option value="TERTEK">Tertek</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama</label>
                <div class="col-sm-8">
                  <input name="nama" class="form-control" placeholder="Nama Lengkap" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Jenis Kelamin</label>
                <div class="col-sm-8">
                  <select required name="jenis_kelamin" class="form-control select2">
                    <option></option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Tempat Lahir</label>
                <div class="col-sm-8">
                  <input name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Tanggal Lahir</label>
                <div class="col-sm-8">
                  <input name="tanggal_lahir" class="form-control datepick" placeholder="Tanggal Lahir" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Golongan Darah</label>
                <div class="col-sm-8">
                  <select required name="gol_darah" class="select2 form-control">
                    <option></option>
                    <option value='A'>A</option>
                    <option value='B'>B</option>
                    <option value='AB'>AB</option>
                    <option value='O'>O</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Agama</label>
                <div class="col-sm-8">
                  <select required name="agama" class="select2 form-control">
                    <option></option>
                    <option value='Buddha'>Buddha</option>
                    <option value='Hindu'>Hindu</option>
                    <option value='Islam'>Islam</option>
                    <option value='Konghucu'>Konghucu</option>
                    <option value='Kristen Katolik'>Kristen Katolik</option>
                    <option value='Kristen Protestan'>Kristen Protestan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status Nikah</label>
                <div class="col-sm-8">
                  <select name="status_nikah" required class="select2 form-control">
                    <option></option>
                    <option value="Cerai Hidup">Cerai Hidup</option>
                    <option value="Cerai Mati">Cerai Mati</option>
                    <option value="Kawin">Kawin</option>
                    <option value="Belum Kawin">Belum Kawin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">SHDRT</label>
                <div class="col-sm-8">
                  <select required name="shdrt" class="select2 form-control">
                    <option></option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Istri">Istri</option>
                    <option value="Anak">Anak</option>
                    <option value="Cucu">Cucu</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Pendidikan Terakhir</label>
                <div class="col-sm-8">
                  <select required name="pddk" class="select2 form-control" placeholder="Pendidikan Terakhir">
                    <option></option>
                    <option value="SD">SD</option>
                    <option value="SMP/MTS">SMP/MTS</option>
                    <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Pekerjaan</label>
                <div class="col-sm-8">
                  <input name="pekerjaan" class="form-control" placeholder="Pekerjaan" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Ibu</label>
                <div class="col-sm-8">
                  <input name="ibu" class="form-control" placeholder="Nama Ibu" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nama Ayah</label>
                <div class="col-sm-8">
                  <input name="ayah" class="form-control" placeholder="Nama Ayah" required type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Alamat</label>
                <div class="col-sm-8">
                  <textarea name="alamat" class="form-control" placeholder="Alamat" required type="text"> </textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="tipe" value="new">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-success">Daftar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>../assets/gambar/1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Penghargaan Kota Tulungagung</h2>
                <p>Baru - baru ini Kota Tulungagung mendapatkan Penghargaan dari Gubernur Jawa Timur. Kota Tulungagung menjadi kota percontohan mulai dari program penghijauan, Kemajuan teknologi dan pembangunan sekolah nelayan</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>../assets/gambar/2.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Program Perempuan Mandiri</h2>
                <p>Program - program dari Kota tulungagung menjadikan usaha mandiri bagi perempuan tulungagung</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>../assets/gambar/3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Pantai Popoh</h2>
                <p>Pantai merupakan salah satu dari sekian banyak destinasi wisata di Kabupaten Tulungagung yang tersebar di wilayah selatan Kabupaten Tulungagung. Ada lebih dari 10 pantai yang terkenal termasuk Pantai Popoh dan beberapa lainnya yang belum ter ekspos.</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->