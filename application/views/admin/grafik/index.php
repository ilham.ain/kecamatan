<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Grafik Kecamatan Tulungagung</h3></header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-white bg-danger">Data Grafik</div>
          <div class="card-body">
            <select class="form-control" id="grafik-judul">
              <option></option>
              <?php foreach ($result as $row) { ?>
                  <option value="<?= $row['id_judul'] ?>"><?= $row['judul'] ?></option>
              <?php } ?>
            </select>              
            <div id="chart-header">
              <canvas id="chart"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="<?= base_url('assets/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
  <script src="<?= base_url('assets/plugins/Chartjs/Chart.bundle.min.js') ?>"></script>
  <script>
    $.ajax({
        url : '<?= base_url('grafik/daftar') ?>',
        method : 'get',
        dataType : 'json',
        success : function(msg) {
            html = '';
            $.each(msg, function(data, row, index){
                html += '<option value="' + row['id_judul'] + '">' + row['judul'] + '</option>';
            });
            $('#grafik-judul').append(html);
        }
    });
    $('#grafik-judul').change(function(){
        $('#chart').remove();
        $('#chart-header').append('<canvas id="chart"><canvas>');
        $.ajax({
            url : '<?= base_url('grafik/get') ?>/' + $(this).val(),
            method : 'get',
            dataType : 'json',
            success : function(msg) {
            var ctx = $('#chart').get(0).getContext('2d')
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: msg['data'],
                options: {
                        responsive: true,
                        title:{
                            display:true,
                            text: msg['text']
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                            }],
                            yAxes: [{
                                display: true,
                            }]
                        }
                    }
            });
            }
        })
    })
  </script>
<?php $this->load->view('admin/partial/footer') ?>