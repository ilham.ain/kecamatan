<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Pendaftaran</h3></header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-white bg-danger">Verifikasi</div>
          <div class="card-body">
            <form method="post" action="" enctype="multipart/form-data">
              <div class="panel-body">
                <div class="form-group">
                    <label>NIK</label>
                    <input type="hidden" name="id_daftar" value="<?= $result['id_daftar'] ?>">
                    <input type="text" name="nik" value="3504014107300075" class="form-control" placeholder="NIK" readonly />
                  </div>
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" value="<?= $result['nama'] ?>" class="form-control" placeholder="Nama" readonly />
                  </div>
                  <div class="form-group">
                    <label>Desa</label>
                    <input type="text" name="desa" value="<?= $result['desa'] ?>" class="form-control" placeholder="Desa " readonly />
                  </div>
                  <div class="form-group">
                    <label>Pelayanan</label>
                    <input type="text" name="pelayanan" value="<?= $result['pelayanan'] ?>" class="form-control" placeholder="Pelayanan " readonly />
                  </div>
                  <div class="form-group">
                    <label>Keperluan</label>
                    <input type="text" name="keperluan" value="<?= $result['keperluan'] ?>" class="form-control" placeholder="Keperluan " />
                  </div>
                  <div class="form-group">
                    <button class="btn btn-success pull-right"><i class="fa fa-save"></i> Verifikasi</button>
                  </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>
<script>
  $('table').dataTable();
</script>