<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Pendaftaran</h3></header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-white bg-danger">Daftar Pengajuan</div>
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>Pelayanan</td>
                  <td>NIK</td>
                  <td>Nama</td>
                  <td>Kelurahan</td>
                  <td>Tanggal Daftar</td>
                  <td width="1px"></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($result as $row) { ?>
                  <tr>
                    <td><?= $row['pelayanan'] ?></td>
                    <td><?= $row['nik'] ?></td>
                    <td><?= $row['nama'] ?></td>
                    <td><?= $row['desa'] ?></td>
                    <td><?= $row['tgl_daftar'] ?></td>
                    <?php if($this->session->userdata('kantor') == 'Kecamatan') { ?>
                      <td><a href="<?= base_url('admin/pendaftaran/verifikasi/'. $row['id_daftar']) ?>">Verifikasi</a></td>
                    <?php } else {?>
                      <?php if($row['status'] == 'sudah diverifikasi di Kelurahan') { ?>
                      <td><a href="<?= base_url('admin/pendaftaran/cetak/'. $row['id_daftar']) ?>">Cetak</a></td>
                      <?php } else if($row['status'] == 'belum diverifikasi') { ?>
                      <td><a href="<?= base_url('admin/pendaftaran/verifikasi/'. $row['id_daftar']) ?>">Verifikasi</a></td>
                      <?php } else { ?>
                      <td><?= $row['status'] ?></td>
                      <?php } ?>
                    <?php } ?>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>
<script>
  $('table').dataTable();
</script>