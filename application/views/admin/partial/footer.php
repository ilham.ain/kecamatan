  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Kecamatan Tulungagung 2018</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Website <a href="https://bootstrapmade.com/">P A T E N</a> by Anonymous
      </div>
    </div>
  </footer>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <script src="<?= base_url('assets/bizpage/lib/jquery/jquery.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/jquery/jquery-migrate.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/easing/easing.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/superfish/hoverIntent.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/superfish/superfish.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/wow/wow.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/waypoints/waypoints.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/counterup/counterup.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/owlcarousel/owl.carousel.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/isotope/isotope.pkgd.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/lightbox/js/lightbox.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/lib/touchSwipe/jquery.touchSwipe.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/contactform/contactform.js') ?>"></script>
  <script src="<?= base_url('assets/AdminLTE/plugins/ckeditor/ckeditor.js') ?>"></script>
  <script src="<?= base_url('assets/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
  <script src="<?= base_url('assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
  <script src="<?= base_url('assets/bizpage/js/main.js') ?>"></script>
  <script>
    $('.tbl-daftar').dataTable({
      fnInfoCallback : function(oSettings, iStart, iEnd, iMax, iTotal, sPre) {
        return iStart + " to " + iEnd + " of " + iMax;
      },
      ordering : false
    });
    $.ajax({
      type: "get",
      url : "<?= base_url('admin/pelayanan/get_pelayanan') ?>",
      success : function (msg) {
        $('#row_num_pelayanan').text("  " + msg + "  ")
      }
    })
    CKEDITOR.replace('ckeditor');
  </script>
  <?php if(!isset($result['home'])) { ?>
  <script>
    $(document).ready(function() {
      $('#header').addClass('header-scrolled');
      $(window).unbind('scroll');
      $('#about').css('padding-top', '90px');
    });
  </script>
  <?php } ?>
  
</body>
</html>
