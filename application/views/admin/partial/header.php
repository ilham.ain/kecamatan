<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
    Pelayanan Kecamatan
  </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="icon">
  <link href="<?= base_url('assets/bizpage/img/logo.png') ?>" rel="logo">
  <link href="<?= base_url('assets/bizpage/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/animate/animate.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/bizpage/lib/lightbox/css/lightbox.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/AdminLTE/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" >
  <link href="<?= base_url('assets/AdminLTE/plugins/iCheck/square/blue.css') ?>">
  <link href="<?= base_url('assets/AdminLTE/plugins/select2/select2.min.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/bizpage/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/adminlte/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/custom/mystyle.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/adminlte/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" />
  <style type="text/css">
    .card .card-header {
      border-radius: 0px;
    }
  </style>
</head>
<body>
  <header id="header">
    <div class="container-fluid">
      <div id="logo" class="pull-left">
        <h1><a href="<?= base_url() ?>" class="scrollto">PATEN</a></h1>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="<?= base_url('admin') ?>">Beranda</a></li>
          <li><a href="<?= base_url('admin/anggota') ?>">Anggota</a></li>
          <li><a href="<?= base_url('admin/grafik') ?>">Grafik</a></li>
          <li><a href="<?= base_url('admin/berita') ?>">Berita</a></li>
          <li><a href="<?= base_url('admin/artikel') ?>">Artikel</a></li>
          <!-- <li><a href="#">Pengaduan</a></li> -->
          <li><a href="<?= base_url('admin/pendaftaran') ?>">Pendaftaran<span class="badge badge-success" id="row_num_pelayanan"></span></a></li>
          <!-- <li><a href="#">Laporan</a></li> -->
          <li class="menu-has-children"><a href="#"><?= $this->session->userdata('kantor') ?></a>
            <ul>
              <li><a href="<?= base_url('admin/setting') ?>">Pengaturan</a></li>
              <li><a href="<?= base_url('admin/logout') ?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </header>

 
  