<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Berita</h3></header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-white bg-danger">Tambah Berita</div>
          <div class="card-body">
            <form action="<?= base_url('admin/berita/update/'. $result['id_berita']) ?>" enctype="multipart/form-data" method="post">
              <div class="form-group">
                <label>Judul Berita</label>
                <input type="text" name="judul" class="form-control" value="<?= $result['judul'] ?>">
              </div>
              <div class="form-group">
                <label>Isi Berita</label>
                <textarea name="isi" class="form-control" id="ckeditor" name="" rows="10" cols="80"><?= $result['isi'] ?></textarea>
              </div>
              <div class="form-group">
                <label>Foto Berita</label>
                <input name="gambar" type="file" class="form-control">
              </div>
              <div class="form-group">
                <button type="btn" class="btn btn-danger" style="border-radius: 0px">Hapus</button>
                <button type="simpan" class="btn btn-success pull-right" style="border-radius: 0px">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>