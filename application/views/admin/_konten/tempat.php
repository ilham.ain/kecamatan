<!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Tempat dan Waktu Pelayanan</h3>
          <p>Silahkan melihat tempat dan waktu pelayanan.</p>
        </header>

        <div class="row">
          <div class="box wow bounceInUp" data-wow-duration="1.4s">
              <div class="col-md-8 ">
              <h2 class="page-header"><b>Tempat Pelayanan</b></h2> 
              <ol>
                 <li>Di Kantor Kecamatan   (   Jl. Diponegoro  Telepon   :   0355-321820  )</li>
                <li>Di Kantor Kelurahan Kedungsoko   (  Jl. Raya Kedungsoko, Telepon   :   0355-329943  )</li>
                <li>Di Kantor Kelurahan Tertek   (  Jl. Dr. Sutomo II/33  Telepon   :   (0355)-323809  )</li>
                <li>Di Kantor Kelurahan Karangwaru   (  Jl. Yos Sudarso III/15, Telepon   :   0355-328992  )</li>
                <li>Di Kantor Kelurahan Tamanan   (   Jl. P. Diponegoro V/153   Telepon   :   0355-325863  )</li>
                <li>Di Kantor Kelurahan Jepun   (  Jl. Mastrip No 10 Telepon   :   0355-335088  )</li>
                <li>Di Kantor Kelurahan Bago  (  Jl. I. Gusti Ngurah Rai VII/13 Telepon,  :  0355-322933  )</li>
                <li>Di Kantor Kelurahan Kepatihan   (  Jl. P.Sudirman VII/89, Telepon   :   0355-328950  )</li>
                <li>Di Kantor Kelurahan Kenayan   (  Jl. DR. Wahidin Sudiro Husodo I/16, Telepon   :   0355-325272  )</li>
                <li>Di Kantor Kelurahan Kampungdalem   (  Jl. A. Yani Timur  02 Telepon   :   0355-328254  )</li>
                <li>Di Kantor Kelurahan Kauman   (  Jl. Mayjen Sungkono 66, Telepon   :   0355-321258  )</li>
                <li>Di Kantor Kelurahan Kutoanyar   (  Jl. M.Yamin I/86 , Telepon   :   0355-332493   )</li>
                <li>Di Kantor Kelurahan Sembung   (   Jl. M. Hatta no 08, Telepon   :   0355-325182  )</li>
                <li>Di Kantor Kelurahan Panggungrejo  (   Jl. Sentulan Raya No. 74B Telepon   :   0355-328086  )</li>
                <li>Di Kantor Kelurahan Botoran  (  Jl. Botoran Timur 15 , Telepon   :   0355-325106  )</li>
              </ol>
            </div>  
            <div class="col-md-4">
              <h2 class="page-header"><b>Waktu Pelayanan</b></h2>
              <ul type="1">
                  <li>Hari SENIN   s/d   KAMIS
                 <br>  Pukul 08.00 WIB   s.d   15.00 WIB.
                 <br>  Istirahat 12.00 WIB   -   12.30 WIB.
                </li>
                <li>Hari JUM’AT
                 <br>  Pukul 08.00 WIB   –   14.00 WIB.
                 <br>  Istirahat 11.00 WIB   –  13.00 WIB.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #services -->