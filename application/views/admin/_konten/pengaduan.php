<!--==========================
      Pengaduan Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>PENGADUAN</h3>
          <p>Silahkan isi form pengaduan. Pengaduan Anda akan segera di tanggapi oleh pihak terkait.</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-7 box box-danger">
            ddd
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-4 box">
            <div class="form"><h2>FORM PENGADUAN</h2>
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Kepada</label>
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <label>Komentar</label>
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" class="btn btn-danger pull-right">Send Message</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #pengaduan -->