<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Artikel</h3></header>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header text-white bg-danger">Tambah Artikel</div>
          <div class="card-body">
            <form action="<?= base_url('admin/artikel/insert') ?>" enctype="multipart/form-data" method="post">
              <div class="form-group">
                <label>Judul Artikel</label>
                <input type="text" name="judul" class="form-control">
              </div>
              <div class="form-group">
                <label>Isi Artikel</label>
                <textarea name="isi" class="form-control" id="ckeditor" name="" rows="10" cols="80"></textarea>
              </div>
              <div class="form-group">
                <label>Foto Artikel</label>
                <input name="gambar" type="file" class="form-control">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-success pull-right" style="border-radius: 0px">Tambah</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-white bg-danger">Daftar Artikel</div>
          <div class="card-body">
            <table class="tbl-daftar table-striped">
              <thead>
                <tr>
                  <td width="100%">Judul</td>
                  <td width="1px"></td>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($result as $row) { ?>
                <tr>
                  <?php if(strlen($row['judul']) > 27) { ?>
                  <td><?= substr($row['judul'], 0, 27) . '...'?></td>
                  <?php } else { ?>
                  <td><?= $row['judul'] ?></td>
                  <?php } ?>
                  <td>  
                    <a href="<?= base_url('admin/artikel/edit/'.$row['id_artikel']) ?>" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                  </td>
                </tr>              
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>