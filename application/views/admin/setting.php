<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Pengaturan</h3></header>
    <?php if($this->session->flashdata()) { ?>
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-<?= $this->session->flashdata('alert') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><i class="fa fa-<?= $this->session->flashdata('icon') ?> "></i></strong> <?= $this->session->flashdata('message') ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header text-white bg-danger">Info Desa</div>
          <div class="card-body">
            <form method="post" action="<?= base_url('admin/setting/info') ?>">
              <div class="panel-body">
                <div class="form-group">
                  <label>Lurah</label>
                  <input type="text" name="lurah" class="form-control" value="<?= (isset($result['lurah']) ? $result['lurah'] : '') ?>">
                </div>
                <div class="form-group">
                  <label>NIP</label>
                  <input type="text" name="nip" class="form-control" value="<?= (isset($result['nip']) ? $result['nip'] : '') ?>">
                </div>
                <div class="form-group">
                  <label>Pangkat</label>
                  <input type="text" name="pangkat" class="form-control" value="<?= (isset($result['pangkat']) ? $result['pangkat'] : '') ?>">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control" value="<?= (isset($result['alamat']) ? $result['alamat'] : '') ?>">
                </div>
                <div class="form-group">
                  <label>Kode Pos</label>
                  <input type="text" name="kode_pos" class="form-control" value="<?= (isset($result['kode_pos']) ? $result['kode_pos'] : '') ?>">
                </div>
                <div class="form-group">
                  <label>Telp</label>
                  <input type="text" name="telepon" class="form-control" value="<?= (isset($result['telepon']) ? $result['telepon'] : '') ?>">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-white bg-danger">Password</div>
          <div class="card-body">
            <form method="post" action="<?= base_url('admin/setting/password') ?>">
              <div class="panel-body">
                <div class="form-group">
                  <label>Password Lama</label>
                  <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                  <label>Password Baru</label>
                  <input type="password" name="new_password" class="form-control">
                </div>
                <div class="form-group">
                  <label>Ketik Ulang Password Baru</label>
                  <input type="password" name="password_confirm" class="form-control">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>