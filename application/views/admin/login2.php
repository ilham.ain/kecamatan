<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Favicons -->
    <link href="<?php echo base_url(); ?>assets/bizpage/img/logo.png" rel="icon">
    <link href="<?php echo base_url(); ?>assets/bizpage/img/logo.png" rel="logo">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/bizpage/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" >
    <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/iCheck/square/blue.css">

    <link href="<?php echo base_url(); ?>assets/AdminLTE/plugins/select2/select2.min.css" rel="stylesheet" />


    <!-- Main Stylesheet File -->
    <link href="<?php echo base_url(); ?>assets/bizpage/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/adminlte/plugins/datepicker/datepicker3.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/adminlte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="index2.html"><b>PATEN</b>KECAMATAN</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Login untuk menggunakan aplikasi</p>
        <form action="#" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- JavaScript Libraries -->
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/waypoints/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/counterup/counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/lightbox/js/lightbox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bizpage/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
    <!-- Contact Form JavaScript File -->
    <script src="<?php echo base_url(); ?>assets/bizpage/contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="<?php echo base_url(); ?>assets/bizpage/js/main.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/AdminLTE/dist/js/app.min.js"></script>

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>