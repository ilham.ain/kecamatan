<?php $this->load->view('admin/partial/header') ?>
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators"></ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" style="background-image: url('<?= base_url('assets/gambar/1.jpg') ?>')">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Penghargaan Kota Tulungagung</h2>
                <p>Baru - baru ini Kota Tulungagung mendapatkan Penghargaan dari Gubernur Jawa Timur. Kota Tulungagung menjadi kota percontohan mulai dari program penghijauan, Kemajuan teknologi dan pembangunan sekolah nelayan</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>
          <div class="carousel-item" style="background-image: url('<?= base_url('assets/gambar/2.jpg') ?>')">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Program Perempuan Mandiri</h2>
                <p>Program - program dari Kota tulungagung menjadikan usaha mandiri bagi perempuan tulungagung</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>
          <div class="carousel-item" style="background-image: url('<?= base_url('assets/gambar/3.jpg') ?>')">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Pantai Popoh</h2>
                <p>Pantai merupakan salah satu dari sekian banyak destinasi wisata di Kabupaten Tulungagung yang tersebar di wilayah selatan Kabupaten Tulungagung. Ada lebih dari 10 pantai yang terkenal termasuk Pantai Popoh dan beberapa lainnya yang belum ter ekspos.</p>
                <a href="#featured-services" class="btn-get-started scrollto">Telusuri</a>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer',array('result' => array('home' => true))) ?>
