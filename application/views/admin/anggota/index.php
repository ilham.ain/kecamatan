<?php $this->load->view('admin/partial/header') ?>
  <section id="about">
    <div class="container">
    <header class="section-header"><h3>Anggota</h3></header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header text-white bg-danger">Daftar Anggota</div>
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>No KK</td>
                  <td>No Nik</td>
                  <td>Nama</td>
                  <td>Jenis Kelamin</td>
                  <td>Tempat Lahir</td>
                  <td>Tanggal Lahir</td>
                  <td>Agama</td>
                  <td>Status</td>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->load->view('admin/partial/footer') ?>
<script>
  $.ajax({
    url : '<?= base_url('admin/anggota/get') ?>',
    dataType : 'json',
    type : 'get',
    success : function(msg) {
      $.each(msg, function(data, row, index) {
        html = '<tr>';
          html+= '<td>' + row['nkk'] + '</td>';
          html+= '<td>' + row['nik'] + '</td>';
          html+= '<td>' + row['nama'] + '</td>';
          html+= '<td>' + row['kelamin'] + '</td>';
          html+= '<td>' + row['tempat_lahir'] + '</td>';
          html+= '<td>' + row['tanggal_lahir'] + '</td>';
          html+= '<td>' + row['agama'] + '</td>';
          html+= '<td>' + row['status_nikah'] + '</td>';
        html += '</tr>';
        $('table tbody').append(html);
      });
      $('table').dataTable();
    }
  });
</script>