<div class="container-wrapper">
	<section id="services">
      <div class="container">

      	<header class="section-header wow fadeInUp">
          <h3>Status Pendaftaran</h3>
          <p>status pendaftaran dapat anda lihat disini.</p>
        </header>

        <div class="box-body table-responsive no-padding">
	        <table class="table table-hover">
	          <tr class="active">
	            <td>No Pendaftaran</td>
	            <td>Pelayanan</td>
	            <td>Tgl Daftar</td>
	            <td>Status</td>
	            <td>Langkah Selanjutnya</td>
	          </tr>
	          <?php foreach ($find as $key ){ ?>
	           <tr>
	          	<td><?= $key['id_daftar'] ?></td>
	          	<td><?= $key['nama'] ?></td>
	          	<td><?= $key['tgl_daftar'] ?></td>
	          	<td><?= $key['status'] ?>
	          		<?php 
                            $status=$key['status'];
                            if($status=='belum diverifikasi'){
                                $ket='Silahkan datang ke Kelurahan anda untuk di verifikasi di tingkat kelurahan';
                            }elseif($status=='sudah diverifikasi di Kelurahan'){
                                $ket='Silahkan datang ke Kantor Kecamatan dengan membawa persyaratan dan surata keterangan dari Kelurahan';
                            }elseif($status=='sudah diverifikasi di Kecamatan'){
                                $ket='Selesai Pelayanan Kami di TIngkat Kecamatan';
                            }else{
                                $ket='';
                            }
                        ?>
	          	</td>
	          	<td><?= $ket ?></td>
	          </tr>
	          <?php } ?>
	      </table>
	  </div>
	  	</div>
	</section>
</div>