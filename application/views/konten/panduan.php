<section id="about">
  <div class="container" >
    <header class="section-header wow fadeInUp">
      <h3>Panduan</h3>
      <p>Silahan klik menu tab untuk melihat panduan.</p>
    </header>
    <div class="row">
      <div class="box wow bounceInUp" data-wow-duration="1.4s">
        <ul class="nav nav-tabs responsive-tabs">
          <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">Mendaftarkan diri</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">Cara memilih pelayanan</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#panel3" role="tab">Pelayanan Terpadu di Kantor Kecamatan</a>
          </li>
        </ul>
      <!-- Tab panels -->
      <div class="tab-content">
          <!--Panel 1-->
          <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
              <p>
              <ol>
                <li>
                    Apabila data anda sudah masuk di "Database Sistem" maka anda tinggal klik "Masuk/Daftar" dan pilih tombol "Masuk" dengan menggunakan user : NIK dan password : NIK 
                </li>
                <li>
                  Apabila data anda belum terdaftar di database sistem maka anda tinggal klik "Masuk/Daftar" dan pilih tombol "Daftar"
                </li>
                <li>
                  Silakan isi data anda dengan benar, sesuai dengan dokumen resmi anda. 
                </li>
                <li>
                  Pilih Daftar, berarti data anda sudah terdaftar di "Database Sistem"
                </li>
              </ol></p>
          </div>
          <!--/.Panel 1-->
          <!--Panel 2-->
          <div class="tab-pane fade" id="panel2" role="tabpanel">
              <br>
              <p>
                <ol>
                  <li>Masuk Aplikasi dengan user dan password sesuai dengan NIK anda.</li>
                  <li>Kemudian klik menu pelayanan.</li>
                  <li>pilih salah satu pelayanan yang anda inginkan. </li>
                  <li>Silahkan pilih tab 'mendaftar'. </li>
                  <li>Anda bisa melihat status pelayanan anda dengan cara klik menu 'Status Pelayanan' dan anda akan dituntun langkah apa yang harus dilakukan </li>
                  <li>Anda bisa melihat status pelayanan anda dengan sms ke nomor HP : 081555384747 dengan cara sms : info#NIK , contoh : Contoh : info#3504014107510xxx</li>
                </ol>
              </p>
          </div>
          <!--/.Panel 2-->
          <!--Panel 3-->
          <div class="tab-pane fade" id="panel3" role="tabpanel">
            <br>
            <p>
            <ol>
              <b><li>Perekaman KTP Elektronik ( KTP-el )</b>
                <br>Persyaratan :
                <br>a.  Surat pengantar dari kelurahan;
                <br>b.  Fotocopy KK sebanyak 2 lembar.
              </li>
              <br>
              <b><li>Legalisasi Surat Dispensasi Nikah</b>
                <br>Melegalisasi surat dispensasi nikah bagi yang mengajukan nikah kurang dari 10 hari.
              </li>
              <br>
              <b><li>Legalisasi Surat Dispensasi Nikah</b>
                <br>Memberikan legalisasi terhadap beberapa jenis surat seperti :
                  <br>a.  Surat Keterangan Catatan Kepolisian (SKCK);
                  <br>b.  Surat Keterangan Tidak Mampu (SKTM);
                  <br>c.  Surat Keterangan Warisan;
                  <br>d.  Surat Keterangan Bepergian;
                  <br>e.  Surat Ijin Keramaian / Kegiatan;
                  <br>f.  Surat Pengajuan IMB/HO;
                  <br>g.  Surat Ijin Usaha Mikro Kecil (IUMK)
                  <br>h.  Surat Pengajuan Taspen dll. 
              </li>
              <br>
              <b><li>Surat Pindah Kependudukan</b>
                <br>Menerbitkan dan membuat pengantar surat pindah dengan kriteria :
                  <br>a.  Pindah antar desa/kelurahan dalam satu kecamatan;
                  Persyaratan :
                  Surat Pengantar dari kelurahan disertai pas foto 4x6 sebanyak 4 lembar
                  <br>b.  Pindah antar desa/kelurahan ke desa/kelurahan lain kecamatan dalam satu kabupaten;
                  Persyaratan :
                  Surat Pengantar dari kelurahan disertai pas foto 4x6 sebanayak 5 lembar
                  <br>c.  Pindah antar kabupaten dalam provinsi dan luar provinsi ;
                  Persyaratan surat pindah :
                  Surat pengantar dari kelurahan, SKCK dan pas foto berwarna 4x6 cm sebanyak 5 lembar.
                  <br>d.  Surat pindah poin c dikeluarkan oleh Dinas Kependudukan dan Pencatatan Sipil
              </li>
              <br>
              <b><li>PPAT</b>
                <br>Berdasarkan Peraturan Pemerintah Republik Indonesia nomor 37 Tahun 1978 bahwa Camat karena jabatannya  ditunjuk sebagai PPAT Sementara di wilayahnya. Dalam pelayanan bidang  Pertanahan di kantor Kecamatan Camat sebagai PPAT memberikan pelayanan kepada masyarakat yang akan mutasi tanah atau pengalihan tanah antara lain :
                <br>1.&nbsp;  Jual beli tanah;
                <br>2.&nbsp;  Hibah;
                <br>3.&nbsp;  Pembagian hak bersama;
                <br>4.&nbsp;  Surat keterangan warisan
                <br>Persayaratan :
                <br>1.&nbsp;  Surat permohonan (blanko model a ) dari kantor pertanahan;
                <br>2.&nbsp;  Blanko akta tanah (jual beli, hibah, pembagian hak bersama);
                <br>3.&nbsp;  Surat kuasa bermaterai (apabila pengurusannya dikuasakan);
                <br>4.&nbsp;  SPPT Tahun terakhir;
                <br>5.&nbsp;  Bukti pembayaran BPHTB/SSB bila harga lebih dari Rp. 60.000.000;
                <br>6.&nbsp;  Bukti nihil apabila harga dibawah Rp. 60.000.000;
                <br>7.&nbsp;  Bukti pembayaran PPH/SSP bila harga lebih dari Rp. 60.000.000.
                <br>8.&nbsp;  Fotocopy KTP suami dan isteri pemohon;
                <br>9.&nbsp;  Fotocopy KTP suami dan isteri yang mengalihkan;
                <br>10.&nbsp; Fotokopi surat nikah suami-isteri pemohon dan yg mengalihkan;
                <br>11.&nbsp; Fotokopy KK pemohon dan yang mengalihkan;
                <br>12.&nbsp; Apabila yang mengalihkan sudah meninggal harus dilampiri : 
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Surat keterangan pewaris;
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Foto kopy KK ahli waris;
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Surat kematian.
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>