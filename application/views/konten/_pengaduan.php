<section id="profile">
  <div class="container" style="padding-top: 80px;">
    <div class="section-header">
        <h3>PENGADUAN</h3>
            <p>Silahkan isi form pengaduan. Pengaduan Anda akan segera di tanggapi oleh pihak terkait.</p>
        </div>
          <div class="row">
            <div class="col-sm-8 pull-left">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Daftar Pengaduan</h3></div>
                        <?php if ($pengaduan) { ?>
                          <?php foreach($pengaduan as $data) {  ?>
                            <div class="box box-widget">
                                <div class="box-header box-danger with-border">
                                    <div class="user-block">
                                    <span class="username" style="margin-left: 0px; color: red;">
                                        <a href="detail_berita.php?pengaduan=<?= $data['id'] ?>">
                                            <?= ucwords($data['nama']) ?> > <?= ucwords($data['kepada']) ?>
                                        </a>
                                    </span>
                                    <span class="description" style="margin-left: 0px;">
                                        <?= date('d-M-Y', strtotime($data['tanggal_pengaduan'])); ?>
                                    </span>
                                    </div>
                                      <!-- /.user-block -->
                                    <div class="box-tools">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <?= $data['komentar'] ?>
                                    </div>
                                    <div class="box-footer">
                                        <h4>Jawaban:</h4>
                                        <?php  
                                           if ($komentar) {
                                            foreach ($komentar as $row) {
                                                ?>
                                                <div class="user-block">
                                                    <span class="username" style="margin-left: 0px;">
                                                        <?= ucwords($row['nama']) ?>
                                                    </span>
                                                    <span class="description" style="margin-left: 0px;">
                                                        <?= date('d-M-Y', strtotime($row['tanggal_pengaduan'])); ?>
                                                    </span>
                                                    <?= $row['komentar'] ?>
                                                </div>
                                                <hr>
                                                <?php 
                                                 } 
                                            } else { ?>
                                            Belum ada komentar
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="box box-widget">
                        <div class="box-header with-border">
                            Belum ada pengaduan
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
   
        <!--***************
            *Form Komentar*
            ***************-->
            <div class="col-sm-4 pull-right">
                <form method="post" action="<?= base_url()?>user/konten_/pengaduan/tambah" name="form-pengaduan" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Form Pengaduan</h3></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2">Nama</label><br>
                                <div class="col-sm-12">
                                    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Pengadu" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2">Kepada</label><br>
                                <div class="col-sm-12">
                                    <input type="text" name="kepada" class="form-control" placeholder="Masukkan Tujuan Aduan" />
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-sm-2">Email</label><br>
                                    <div class="col-sm-12">
                                        <input type="text" name="email" class="form-control" placeholder="Masukkan Email" />
                                </div>
                            </div>
                            <?php
                                $tgl=date('d-m-Y');
                            ?>                    
                            <input type="hidden" name="tanggal_pengaduan" hidden class="form-control" value="<?= $tgl; ?>">
                            
                            <div class="form-group">
                                <label class="col-sm-2">Komentar</label><br>
                                <div class="col-sm-12">
                                    <textarea name="komentar" rows="12" class="form-control" placeholder="Masukkan Komentar"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                            <button class="btn btn-danger pull-right" type="submit"><i class="fa fa-save"></i> Adukan</button>
                        </div>
                    </div>
                </form>
                        
            </div>
        </div>
    </div>
</section>
