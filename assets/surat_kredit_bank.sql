-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2018 at 06:35 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kecamatan`
--

-- --------------------------------------------------------

--
-- Table structure for table `surat_kredit_bank`
--

CREATE TABLE `surat_kredit_bank` (
  `id_surat` int(11) NOT NULL,
  `nik` varchar(17) NOT NULL,
  `keterangan` mediumtext NOT NULL,
  `alamat` text NOT NULL,
  `keperluan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_kredit_bank`
--

INSERT INTO `surat_kredit_bank` (`id_surat`, `nik`, `keterangan`, `alamat`, `keperluan`) VALUES
(1, '3504132307050002', 'menggadaikan montor honda', 'kelurahan bago', 'modal usaha ternak ikan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `surat_kredit_bank`
--
ALTER TABLE `surat_kredit_bank`
  ADD PRIMARY KEY (`id_surat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `surat_kredit_bank`
--
ALTER TABLE `surat_kredit_bank`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
